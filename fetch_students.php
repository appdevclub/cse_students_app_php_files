<?php

$response = array();

require_once __DIR__ . '/connect.php';

$db = new DB_CONNECT();
$connect = $db->dbconnect();

if (isset($_GET["facultyid"])) {
    $facultyid = $_GET['facultyid'];
	$result = mysqli_query($connect,"SELECT * FROM staff_details WHERE facultyid=$facultyid");
	 
    if ($result) {
	
		$response["success"] = 1;
        
		while($staffdetails = $result->fetch_object()) {
		
			$sections = explode(",",$staffdetails->sections);
			$response["students"] = array();
									
			foreach($sections as $sect) {
				
				$details = mysqli_query($connect,"SELECT * FROM students_details WHERE section='$sect'");
								
				if($details) {
					$students["section"] = $sect;
					$students["students"] = array();
					while($s = $details->fetch_object()) {
						$student = array();
						$student["regno"] = $s->regno;
						$student["name"] = $s->name;
						$student["phone"] = $s->phone;
						array_push($students["students"], $student);
					}
					
				}
				array_push($response["students"], $students);
			}
			                        
            echo json_encode($response);
		}
        
    } else {
        $response["success"] = 0;
        $response["message"] = "No students found";
        echo json_encode($response);
    }
} else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}
$db->close($connect);

?>