<?php
$response = array();

require_once __DIR__ . '/connect.php';

$db = new DB_CONNECT();
$connect = $db->dbconnect();

if (isset($_GET["regno"])) {
    $regno = $_GET['regno'];
	$result = mysqli_query($connect,"SELECT * FROM timetable WHERE start_regno<=$regno and end_regno>=$regno");
 
    if ($result) {
        
		while($timetable = $result->fetch_object()) {
                        
            $response["success"] = 1;
             
            $response["timetable"] = array();
 
            array_push($response["timetable"], $timetable);
 
            echo json_encode($response);
		}
        
    } else {
        $response["success"] = 0;
        $response["message"] = "No timetable found";
        echo json_encode($response);
    }
} else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}
$db->close($connect);
?>