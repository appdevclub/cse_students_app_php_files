<?php
$response = array();

require_once __DIR__ . '/connect.php';

$db = new DB_CONNECT();
$connect = $db->dbconnect();

if (isset($_GET["regno"])) {
    $regno = $_GET['regno'];
	
	$result = mysqli_query($connect,"SELECT * FROM timetable WHERE start_regno<=$regno and end_regno>=$regno");
	$result = $result->fetch_object();
	$section = $result->section;
		
    $result = mysqli_query($connect,"SELECT * FROM staff_details WHERE sections LIKE '%$section%'");
 
    if ($result) {
	
		 $response["success"] = 1;
             
        $response["teachers"] = array();
         
		while($list = $result->fetch_object()) {
             
            $teachers = array();
            $teachers["name"] = $list->name;
            $teachers["position"] = $list->position;
			$teachers["room_no"] = $list->room_no;
			$teachers["phone_no"] = $list->phone;
			$sections = explode(",",$list->sections);
			$subjects = explode(",",$list->subjects);
			$i=0;
			foreach ($sections as $sect) {
				if (strcasecmp($sect,$section) == 0) {
					$teachers["subject"] = $subjects[$i];
					break;
				}
				$i++;
			}
            
            array_push($response["teachers"], $teachers);
           
        }
		echo json_encode($response);
    } else {
        $response["success"] = 0;
        $response["message"] = "No faculties found";
        echo json_encode($response);
    }
} else {
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
    echo json_encode($response);
}
?>